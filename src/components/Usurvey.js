import React, {Component} from 'react';
const firebase = require('firebase');
const uuid = require('uuid');

const config = {
    apiKey: "AIzaSyBwakntVXzETozhJH1m-fwWbsNSiyJ6jis",
    authDomain: "u-survey-b3fe3.firebaseapp.com",
    databaseURL: "https://u-survey-b3fe3.firebaseio.com",
    projectId: "u-survey-b3fe3",
    storageBucket: "u-survey-b3fe3.appspot.com",
    messagingSenderId: "267091307845"
};
firebase.initializeApp(config);


class Usurvey extends Component {
    constructor(props) {
        super(props);

        this.state = {
          uid: uuid.v1(),
            studentName: '',
            answers: {
                answer1: '',
                answer2: '',
                answer3: ''
                },
            isSubmitted: false
        };

        // this.nameSubmit = this.nameSubmit.bind(this);
        // this.selectAnswers = this.selectAnswers.bind(this);
    }

    selectAnswers = (evt) => {
     const {answers} = this.state;
     if (evt.target.name === 'answer1') {
         answers.answer1 = evt.target.value;
     } else if(evt.target.name === 'answer2') {
         answers.answer2 = evt.target.value;
     } else if(evt.target.name === 'answer3') {
         answers.answer3 = evt.target.value;
        }

        this.setState({
            answers
        }, () => {
         console.log(this.state);
        });

    };

    nameSubmit = (evt) => {
        let studentName = this.inputName.value;
        this.setState({studentName}, () => {
            console.log(this.state);
        });

    };

    questionSubmit = () => {
        firebase.database().ref(`uSurvey/${this.state.uid}`).set({
        studentName: this.state.studentName,
            answers: this.state.answers
        });
        this.setState({
           isSubmitted: true
        });

    };

    render() {
        let studentName;
        let questions;
        if (this.state.studentName === '' &&  this.state.isSubmitted === false) {
            studentName = <div>
                <h1>Hey Student, please let us know your name: </h1>
                <form onSubmit={this.nameSubmit}>
                    <input className="name-input" type="text" placeholder="Enter your name" ref={ (inputName) => this.inputName = inputName} />
                </form>
            </div>;
                    questions = '';
        } else if(this.state.studentName !== '' && this.state.isSubmitted === false ) {
            studentName = <h1> Welcome to Survey, {this.state.studentName} </h1>;
            questions = <div>
               <h2>Here is some questions: </h2>
                <form onSubmit={this.questionSubmit}>
                    <div className="card">
                        <label htmlFor="">What kind of backend frameworks you like?</label> <br/>
                        <input type="radio" name="answer1" value="Rails" onChange={this.selectAnswers}/> Rails
                        <input type="radio" name="answer1" value="Django" onChange={this.selectAnswers}/> Django
                        <input type="radio" name="answer1" value="Nodejs" onChange={this.selectAnswers}/> Nodejs
                       </div>
                    <div className="card">
                        <label htmlFor="">What is your favorite frontend library?</label> <br/>
                        <input type="radio" name="answer2" value="React" onChange={this.selectAnswers}/> React.js
                        <input type="radio" name="answer2" value="Angular" onChange={this.selectAnswers}/> Angular 4
                        <input type="radio" name="answer2" value="Vue" onChange={this.selectAnswers}/> Vue.js 2
                    </div>
                    <div className="card">
                        <label htmlFor="">What is your favorite web developing type of programming??</label> <br/>
                        <input type="radio" name="answer3" value="Front" onChange={this.selectAnswers}/> Frontend
                        <input type="radio" name="answer3" value="Back" onChange={this.selectAnswers}/> Backend
                        <input type="radio" name="answer3" value="Full" onChange={this.selectAnswers}/> I want to be fulls tack
                    </div>

                    <div>
                        <input type="submit" className="feedback-button" value="submit"/>
                    </div>
                </form>
            </div>;
        } else if(this.state.isSubmitted === true) {
            studentName = <h1> Thanks, {this.state.studentName}</h1>;
        }
        return (
            <div>
                {studentName}
                ----------------------------------------
                {questions}
            </div>
        );
    }
}



export default Usurvey;
